# discuss @ tcvar via excel col mapping
the testsuite creation seems to be redundant at first sight/use - so why need a testsuite? why not only go with testcase?
we call katalon testcase   as ktlon tc
we call katalon test suite as ktlon ts

in my view, thanks to [trangtruong1610's demo](https://github.com/trangtruong1610/katalon-start/blob/master/doc/map_excel_column_to_testcase_variable.png), 
we can take ktlon tc and testsuite this way

00  define **ktlon tc** ie  `steps running against variables` for input and expected output ie `no specific values here yet`
    we call variables defined in a ktlon tc as tcvar 

01 in **ktlon ts** we map tcvar with value in excel aka we `map tcvar vs excel col name in ktlon ts` 
