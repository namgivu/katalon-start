#!/bin/bash
SH=`cd $(dirname $BASH_SOURCE) && pwd`
AH=`cd $SH/.. && pwd`

    PYTHONPATH=`pwd`  pipenv run python "$AH/src/app.py"
