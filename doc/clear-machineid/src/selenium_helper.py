from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

import time

from webdriver_vault.downloaded_chrome_wd.wd import WD_IMPLICITLY_WAIT


def nap(howlong=1):
    time.sleep(howlong)


WD_WAIT_TIMEOUT = WD_IMPLICITLY_WAIT


def wait_name(wd, name):
    return WebDriverWait(wd, WD_WAIT_TIMEOUT).until(EC.visibility_of_element_located((By.NAME, name)))


def wait_class(wd, name):
    return WebDriverWait(wd, WD_WAIT_TIMEOUT).until(EC.visibility_of_element_located((By.CLASS_NAME, name)))


def wait_xpath(wd, xpath):
    return WebDriverWait(wd, WD_WAIT_TIMEOUT).until(EC.visibility_of_element_located((By.XPATH, xpath)))


def wait_css(wd, css):
    return WebDriverWait(wd, WD_WAIT_TIMEOUT).until(EC.visibility_of_element_located((By.CSS_SELECTOR, css)))


def scroll_to_bottom(wd):
    wd.execute_script('window.scrollTo(0, document.body.scrollHeight);')  # scroll to bottom of page
