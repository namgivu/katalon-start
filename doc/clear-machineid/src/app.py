import dotenv
import os

from webdriver_vault.downloaded_chrome_wd.wd import load_webdriver_f_downloaded_chromewd
from src.selenium_helper import wait_name, nap, wait_xpath, scroll_to_bottom, wait_css


MACHINE_ID_KEPT = os.environ.get('MACHINE_ID_TO_KEEP'); MACHINE_ID_KEPT = [] if MACHINE_ID_KEPT is None else MACHINE_ID_KEPT.strip().split(' ')

wd = load_webdriver_f_downloaded_chromewd()  # wd aka webdriver
try:
    #region login katalon
    SH=os.path.dirname(__file__); dotenv.load_dotenv(f'{SH}/.env')
    LOGIN_USER = os.environ.get('LOGIN_USER')
    LOGIN_PSWD = os.environ.get('LOGIN_PSWD')

    wd.get('https://analytics.katalon.com/login')
    e_user = wait_name(wd, 'username'); e_user.send_keys(LOGIN_USER)
    e_pswd = wait_name(wd, 'password'); e_pswd.send_keys(LOGIN_PSWD)

    e_loginbutton = wait_xpath(wd, '//button[text()="Sign in"]'); e_loginbutton.click()
    #endregion login katalon

    nap(3)  # wait 3 sec to finish loading after-logged-in screen

    while True:  # loop until :remove_happened is False
        remove_happened = False

        #region collect machineid
        URL__ORG_ADMIN_LICENSE_KEY = os.environ.get('URL__ORG_ADMIN_LICENSE_KEY')
        wd.get(URL__ORG_ADMIN_LICENSE_KEY)

        scroll_to_bottom(wd) ; e_machineid_list = wd.find_elements_by_css_selector('#machine tr div')
        scroll_to_bottom(wd) ; e_rmbutton_list  = wd.find_elements_by_css_selector('#machine tr button')
        assert len(e_machineid_list) == len(e_rmbutton_list)
        #endregion


        #region delete machineid
        for i, e_machineid in enumerate(e_machineid_list):
            machineid = e_machineid.text
            if machineid not in MACHINE_ID_KEPT:  # only delete if not listed in kept-listing
                # click-then-confirm to remove
                print(f'Deleting {machineid} ...')
                e_rmbutton_list[i].click(); e_remove=wait_xpath(wd, '//button[text()="Remove"]'); e_remove.click() ; nap()

                remove_happened = True

                break  # we must re-locate element with :wd cause continue with old element get error > selenium.common.exceptions.StaleElementReferenceException: Message: stale element reference: element is not attached to the page document
        #endregion delete machineid

        if not remove_happened:
            break

finally:  # always quit() no matter code run result as pass or exception
    wd.quit()
