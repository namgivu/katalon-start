# context
we have a testee behind a VPN 
we automate the test with selenium in katalon using remote webdriver 


# terms
word          aka
-----------   --------------------
testee        the tested one
wd            webdriver 
ktlon         katalon 
brs           browserstack 
desired cap   desired capabilities 


# solution 
main idea
ref. email to ask for support from browserstack team
> use BrowserStack local testing feature along with the force-local parameter

00 setup a custom desired cap in ktlon that point to brs wd - TODO add guide url for this 
01 run BrowserStackLocal with your secret - this will help to route your local network to brs server and allow your script to use it to bypass any VPN if exists 
