# goal
make a google search for keyword :katalon and take snapshot of the result page

# steps as newbie
00  run katalon studio aka katalon ide aka ktlon

01  run `Record Web` via 
    - toolbar icon aka the globe with red dot
    - menu Action - Record - Record Web
    [Web Record](./webrecord-popup.png) will popup

02  browse the google search web normally as you normally do it with below notes 
    - using web browser as **ktlon popup browser**
    -
    - do spy/record web DOM element aka **ktlon object** via `alt-backquote` - this can be customizable to ctrl-backquote [via menu Windows - Ktlon Preference](helpul-tip/spyobject-hotkey-custom.png) 
      spying them down so that our automate script clearer; otherwise, we see xpath and not knowing what the element is
      spy object has name so that we know what elam we working with 
    -
    - take snapshot by adding ktlon action `Take Screenshot` in `Web Record popup` whenever we want to
      snapshot only visible when run with `ktlon testsuite` under :ktlon_project/Reports/YYYYmmDD_HHMMSS/:tcname
 
02b hit OK at the bottom, of `Web Record popup`, when done
    choose the testcase name aka tcname to save and where to store the spied objects
